# WebREST
Este projeto foi desenvolvido por Diego Herculano na linguagem PHP, com o framework Laravel 7 e a biblioteca JWT.
As suas devidas documentações podem ser encontradas em (https://laravel.com/ e https://jwt-auth.readthedocs.io/en/develop/).

Trata-se de uma API utilizando o conceito REST, com gerenciamento de token e expiração do mesmo.

O conceito desta aplicação é tratar os dados de forma segura, bem como em gerar comunicação direta com o serviço web de maneira dinâmica, trazendo, modificando, e criando um usuário com os dados de nome, e-mail, telefone e senha.

Vídeo demonstrativo: https://drive.google.com/file/d/1LWaPgRFhTHDp5Yuw0j11d4R_30NsyE5T/view?usp=sharing

Instruções:
As urls com saída api/register e api/login geram token de uso bearer, que deve ser usado para as demais urls.

Endereços de consulta padrões (rotas) com autenticação:
    1. api/register (POST)
    2. api/login (POST)
    3. api/logout (POST)
    4. api/user/{id} (GET, DELETE, POST)
    5. api/users (GET)

Instalação:
O uso desta aplicação requer a instalação prévia do PHP 7 ou superior, bem com os requisitos descritos na documentação do Laravel (https://laravel.com/), assim como, o uso imprescindível do programa composer.

Ao realizar o clone do repositório, a maneira mais rápida de usar esta aplicação é gerando o comando: composer install,  na pasta raiz, e após isto configurando o arquivo .env na pasta raiz também, e posteriormente, gerando as migrations do Laravel digitando o comando: php artisan migrate.

